/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.nsco.music.tests;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.os.Bundle;

/**
 * Instrumentation class for Media Player launch performance testing.
 */
public class MusicPlayerLaunchPerformance extends Instrumentation {

	protected Bundle mResults;
	protected Intent mIntent;

	public MusicPlayerLaunchPerformance() {
		this.mResults = new Bundle();
		this.mIntent = new Intent(Intent.ACTION_MAIN);
		this.mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		setAutomaticPerformanceSnapshots();
	}

	/**
	 * Launches intent, and waits for idle before returning.
	 *
	 * @hide
	 */
	protected void LaunchApp() {
		startActivitySync(this.mIntent);
		waitForIdleSync();
	}

	public static final String LOG_TAG = "MusicPlayerLaunchPerformance"; //$NON-NLS-1$

	@Override
	public void onCreate(Bundle arguments) {
		super.onCreate(arguments);

		this.mIntent.setClassName(getTargetContext(), "com.android.nsco.music.MusicBrowserActivity"); //$NON-NLS-1$
		start();
	}

	/**
	 * Calls LaunchApp and finish.
	 */
	@Override
	public void onStart() {
		super.onStart();
		LaunchApp();
		finish(Activity.RESULT_OK, this.mResults);
	}
}
