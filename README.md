# README #

### 開発環境構築手順

## 1.ソース取得

git clone https://takakom2@bitbucket.org/nsco/music.git

## 2.Eclipseによるプロジェクトのインポート

１.ファイル＞インポート＞Exiting Android Code Into Workspace＞次へ

２."ルート・ディレクトリ" に取得したソースのパスを入力する

３.完了

## 3.サポートライブラリの追加

１．インポートしたプロジェクトにマウスポインタを合わせて右クリック

２．Androidツール＞サポートライブラリの追加

３．Accept Licenseのラジオボタンにチェックをつける

４．インストール


## 4.インストール

ビルドして実行(Anroirdアプリケーションとして)し、エミュレータもしくは実機にアプリをインストールする